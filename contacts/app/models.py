from sqlalchemy.exc import SQLAlchemyError
from app import db
import logging
import json


class Contacts(db.Model):
    """
    class representing a Contact
    """
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), nullable=False, unique=True)
    firstname = db.Column(db.String(30), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    emails = db.relationship('Emails', cascade='all,delete', backref='contacts', lazy='dynamic')

    def json(self, emails):
        """
        retuen contact as jason with associated emails
        :param emails:
        :return:
        """
        ret = {'username':self.username, 'firstname': self.firstname, 'surname': self.surname, 'emails': []}
        new_emails = []
        for email in emails:
            new_email = {'email': email.email}
            new_emails.append(new_email)
        ret['emails'] = new_emails
        return ret

    @staticmethod
    def add_contact(_username, _emails, _firstname, _surname):
        """
        add a contact
        :param _username:
        :param _emails:
        :param _firstname:
        :param _surname:
        :return:
        """
        contact = Contacts(username=_username, firstname=_firstname, surname=_surname)
        for email in _emails:
            email = Emails(email=email['email'], contacts=contact)
            db.session.add(email)
        db.session.add(contact)
        try:
            db.session.commit()
        except SQLAlchemyError as e:
            pass
            # TODO log error, return error

    @staticmethod
    def get_all_contacts():
        """
        retrieve all contacts
        :return:
        """
        contacts = Contacts.query.all()
        ret = []
        for contact in contacts:
            emails = Emails.query.filter_by(contacts=contact).all()
            js = Contacts.json(contact, emails)
            ret.append(js)

        return ret

    @staticmethod
    def get_contact_by_username(_username):
        """
        retrieve a contact by username
        :param _username:
        :return:
        """
        contact =  Contacts.query.filter_by(username=_username).first()
        emails = Emails.query.filter_by(contacts=contact).all()
        if contact is not None:
            return Contacts.json(contact, emails)
        else:
            return False

    @staticmethod
    def get_contact_by_email(email):
        """
        retrieve a contact by email address
        :param email:
        :return:
        """
        email = Emails.query.filter_by(email=email).first()
        if email is None:
            return False

        contact =  Contacts.query.filter_by(id=email.contacts_id).first()
        if contact is None:
            return False

        emails = Emails.query.filter_by(contacts=contact).all()

        if contact is not None:
            return Contacts.json(contact, emails)
        else:
            return False

    @staticmethod
    def delete_contact( _username):
        """
        delete a contact
        :param _username:
        :return:
        """
        status = Contacts.query.filter_by(username=_username).delete()
        try:
            db.session.commit()
        except SQLAlchemyError as e:
            status = False
            # TODO log error
        return bool(status)

    @staticmethod
    def update_contact(_username, _emails, _firstname, _surname):
        """
        update a contact
        :param _username:
        :param _emails:
        :param _firstname:
        :param _surname:
        :return:
        """
        contact = Contacts.query.filter_by(username=_username).first()
        Emails.query.filter_by(contacts=contact).delete()
        contact.username = _username
        contact.firstname = _firstname
        contact.surname = _surname
        for email in _emails:
            new_email = email['email']
            email = Emails(email=new_email, contacts=contact)
            db.session.add(email)

        try:
            db.session.commit()
        except SQLAlchemyError as e:
            pass
            # TODO log error, and return error

    def __repr__(self):
        return "username {}, firstname {}, surname {}".format(self.username, self.firstname, self.surname)


class Emails(db.Model):
    """
    class representing the Emails associated with a Contact
    """
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(256), nullable=False, unique=True)
    contacts_id = db.Column(db.Integer, db.ForeignKey('contacts.id'), nullable=False)

    def json(self):
        """
        return Email as json
        :return:
        """
        return {'email': self.email}

    def __repr__(self):
        return "email {}".format(self.email)
