from app import app
from flask import jsonify, request, Response
from .models import Contacts, Emails
import json

def getErrorResponse(error, help):
    ErrorMsg = {
        "error": error,
        "helpString": help
    }
    return ErrorMsg

#GET /contacts
@app.route('/contacts')
def get_contacts():
    return jsonify({'contacts': Contacts.get_all_contacts()})

#GET /contact?username=<username>
#GET /contact?email=<email>
@app.route('/contact')
def get_contact():
    username = request.args.get('username')
    email = request.args.get('email')

    if username:
        return_value = Contacts.get_contact_by_username(username)
    elif email:
        return_value = Contacts.get_contact_by_email(email)
    else:
        error = getErrorResponse("Contact with Username or Email provided not found", "")
        response = Response(json.dumps(error), status=404, mimetype='application/json')
        return response

    if return_value:
        return jsonify(return_value)
    else:
        error = getErrorResponse("Contact with Username or Email provided not found", "")
        response = Response(json.dumps(error), status=404, mimetype='application/json')
        return response

def validContact(contact):
    if contact is not None and "username" in contact and "firstname" in contact and "surname" in contact and \
            "emails" in contact:
        return True
    else:
        return False

#POST /contacts
@app.route('/contacts', methods=['POST'])
def add_contact():
    request_data = request.get_json()


    if validContact(request_data):
        Contacts.add_contact(request_data['username'], request_data['emails'], request_data['firstname'],
                            request_data['surname'])
        response = Response("", status=201, mimetype='application/json')
        response.headers['Location'] = "/books/" + str(request_data['username'])
        return response
    else:
        error = getErrorResponse("Invalid contact object passed in request",
                                 "Data should be {'username': 'Contact Name', 'emails': [{'email':'Contact@gmail.com'}], "
                                 "'firstname': 'Joe', 'surname': 'Blogs' }")
        response = Response(json.dumps(error), status=400, mimetype='application/json')
        return response;


def valid_put_request_data(request_data):
    if "username" in request_data  and "firstname" in request_data \
            and "surname" in request_data:
        return True
    else:
        return False

#PUT /contacts/<string:username>
@app.route('/contacts/<string:username>', methods=['PUT'])
def update_contact(username):
    request_data = request.get_json()

    if(not valid_put_request_data(request_data)):
        error = getErrorResponse("Invalid contact object passed in request",
                                 "Data should be {'username': 'Contact Name',"
                                 " 'emails': [{'email':'Contact@gmail.com'}], "
                                 "'firstname': 'Joe', 'surname': 'Blogs' }")
        response = Response(json.dumps(error), status=400, mimetype='application/json')
        return response

    Contacts.update_contact(username, request_data['emails'], request_data['firstname'], request_data['surname'])
    response = Response("", status=204)
    return response

# DELETE /contacts/<string:username>
@app.route('/contacts/<string:username>', methods=['DELETE'])
def delete_contact(username):

    if (Contacts.delete_contact(username)):
        return Response("", status=204)

    error = getErrorResponse("Contact with Username provided not found, so unable to delete.", "")
    response = Response(json.dumps(error), status=404, mimetype='application/json')
    return response

