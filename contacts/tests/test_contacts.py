import os
import json
import unittest

from app import app, db
from app.models import Contacts, Emails

class TestContactsCase(unittest.TestCase):
    def setUp(self):
        basedir = os.path.abspath(os.path.dirname(__file__))
        app.config['TESTING'] = True
        app.config['WTF_CSRF)ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'test.db')
        self.app = app.test_client()
        db.drop_all()
        db.create_all()
        contact = Contacts(username='DavidU', firstname='David', surname='Urquhart')
        db.session.add(contact)
        email = Emails(email='david@gmail.com', contacts=contact)
        db.session.add(email)
        db.session.commit()


    def tearDown(self):
        pass

    def test_get_all_contacts(self):
        response = self.app.get('/contacts', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_get_contact_by_username(self):
        response = self.app.get('/contact?username=DavidU',
                                follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_get_contact_by_email(self):
        response = self.app.get('/contact?email=david@gmail.com',
                                follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_add_contact(self):
        response = self.app.post('/contacts',
                                 json={"username": "FredM", "firstname": "Fred", "surname": "Ma", "emails": [{"email": "fred@gmail.com"}]},
                                 follow_redirects=True,
                                 content_type='application/json')
        self.assertEqual(response.status_code, 201)
        

    def test_update_contact(self):
        response = self.app.put('/contacts/DavidU',
                                 json={"username": "JoeB", "firstname": "Joe", "surname": "Bloggs", "emails": [{"email": "joe@gmail.com"}]},
                                 follow_redirects=True,
                                 content_type='application/json')
        self.assertEqual(response.status_code, 204)

    def test_delete_contact(self):
        response = self.app.delete('/contacts/DavidU',
                                   follow_redirects=True)
        self.assertEqual(response.status_code, 204)

if __name__ == '__main__':
    unittest.main()

