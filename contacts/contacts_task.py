from celery import Celery
from celery.schedules import crontab
import random
import string
from app import app
from app.models import Contacts, Emails
from celery_sched import make_celery
import json

app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379'
)
   
celery = make_celery(app)

@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):

    sender.add_periodic_task(15.0, generate_random_contacts, name='add contact every 15 seconds')

    sender.add_periodic_task(60.0, delete_contact_after_1_min, name='delete contact emails every minute')

def generateRandomString(max_length=30):
    """
    create random strinf
    :param max_length: maximum length of string to produce
    :return: random string
    """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(max_length))


@celery.task()
def generate_random_contacts():
    """
    create contact every 15 seconds
    :return: 0
    """
    username = generateRandomString(30)
    firstname = generateRandomString(30)
    surname = generateRandomString(50)
    emails = [{'email': generateRandomString(256)},{'email': generateRandomString(256)}]
    contact = Contacts.add_contact(username, emails, firstname, surname)
        
    return 0

@celery.task()
def delete_contact_after_1_min():
    """
    delete contacts every minute
    :return: None
    """
    Contacts.query().all.delete()
